<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:fn="http://www.w3.org/2005/02/xpath-functions"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    version="2.0">

  <xsl:param name="publishedOnly"/>

  <xsl:variable name="pageTitle" select="'Revue de presse'"/>
  <xsl:variable name="editionLinkPrefix" select="'edition-'"/>
  <xsl:variable name="briefLinkPrefix" select="'brief-'"/>
  <xsl:variable name="topicLinkPrefix" select="'topic-'"/>
  <xsl:variable name="editionLabel" select="'Édition'"/>
  <xsl:variable name="topicLabel" select="'Thème'"/>
  <xsl:variable name="htmlUrl"
      select="'https://perso.crans.org/jeltz/revue/'"/>
  <xsl:variable name="atomIriPrefix"
      select="'brief:b0e78058-47a9-4298-a240-c19f66967eb5'"/>
  <xsl:variable name="sourcesAtomHeader" select="'Sources'"/>
  <xsl:variable name="topicsAtomHeader" select="'Thèmes'"/>
  <xsl:variable name="licenceUrl"
      select="'https://creativecommons.org/licenses/by-sa/4.0/legalcode'"/>
  <xsl:variable name="licenceName" select="'CC BY-SA 4.0'"/>
  <xsl:variable name="sourceUrl"
      select="'https://gitlab.crans.org/jeltz/press-review'"/>

  <xsl:function name="fn:toBool" as="xs:boolean">
    <xsl:param name="string" as="xs:string?"/>
    <xsl:param name="default" as="xs:boolean"/>
    <xsl:choose>
      <xsl:when test="string-length($string) > 0">
        <xsl:variable name="lower" select="lower-case($string)"/>
        <xsl:sequence select="$lower = 'true' or $lower = 'yes'"/>
      </xsl:when>
      <xsl:otherwise>
        <xsl:sequence select="$default"/>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:function>

  <xsl:function name="fn:isPublished" as="xs:boolean">
    <xsl:param name="published" as="xs:string?"/>
    <xsl:sequence
        select="fn:toBool($published, true())
            or not(fn:toBool($publishedOnly, false()))"/>
  </xsl:function>

  <xsl:function name="fn:isPaywalled" as="xs:boolean">
    <xsl:param name="paywall" as="xs:string?"/>
    <xsl:sequence select="fn:toBool($paywall, false())"/>
  </xsl:function>

  <xsl:template match="edition" mode="timeText">
    <xsl:value-of
        select="format-date(@date, '[D01] [Mn] [Y0001]', 'fr', 'AD', 'FR')"/>
  </xsl:template>

  <xsl:template match="external" mode="linkText">
    <xsl:value-of select="@href"/>
  </xsl:template>

  <xsl:template match="legifrance" mode="linkText">
    <xsl:text>https://www.legifrance.gouv.fr/</xsl:text>
    <xsl:choose>
      <xsl:when test="boolean(@loda)">
        <xsl:value-of select="concat('loda/id/', @loda, '/')"/>
      </xsl:when>
      <xsl:when test="boolean(@article)">
        <xsl:value-of select="concat('codes/article_lc/', @article, '/')"/>
      </xsl:when>
      <xsl:when test="boolean(@jorf)">
        <xsl:value-of select="concat('jorf/id/', @jorf, '/')"/>
      </xsl:when>
    </xsl:choose>
    <xsl:if test="boolean(@date)">
      <xsl:value-of select="concat(@date, '/')"/>
    </xsl:if>
  </xsl:template>

  <xsl:template match="conseil-etat" mode="linkText">
    <xsl:text>https://www.conseil-etat.fr/fr/arianeweb/CE/decision/</xsl:text>
    <xsl:value-of select="concat(@date, '/', @id)"/>
  </xsl:template>

  <xsl:template match="eur-lex" mode="linkText">
    <xsl:text>https://eur-lex.europa.eu/legal-content/</xsl:text>
    <xsl:value-of select="upper-case(@lang)"/>
    <xsl:text>/TXT/?uri=</xsl:text>
    <xsl:value-of select="@uri"/>
  </xsl:template>

  <xsl:template match="email" mode="linkText">
    <xsl:value-of select="concat('mailto:', @address)"/>
  </xsl:template>

  <xsl:template match="wikipedia" mode="linkText">
    <xsl:value-of
        select="concat('https://', @lang, '.wikipedia.org/wiki/', @title)"/>
  </xsl:template>

  <xsl:template match="*|text()" mode="linkText"/>

  <xsl:template match="source" mode="linkText">
    <xsl:apply-templates mode="linkText"/>
  </xsl:template>

  <xsl:template match="para" mode="miniMarkupHtml">
    <p>
      <xsl:apply-templates mode="miniMarkupHtml"/>
    </p>
  </xsl:template>

  <xsl:template match="para" mode="miniMarkupText">
      <xsl:apply-templates mode="miniMarkupText"/>
      <xsl:text>&#xA;</xsl:text>
  </xsl:template>

  <xsl:template match="emph" mode="miniMarkupHtml">
    <em>
      <xsl:apply-templates mode="miniMarkupHtml"/>
    </em>
  </xsl:template>

  <xsl:template match="emph" mode="miniMarkupText">
      <xsl:text>**</xsl:text>
      <xsl:apply-templates mode="miniMarkupText"/>
      <xsl:text>**</xsl:text>
  </xsl:template>

  <xsl:template
      match="external|eur-lex|legifrance|wikipedia|email|conseil-etat"
      mode="miniMarkupHtml">
    <a rel="external noreferrer">
      <xsl:attribute name="href">
        <xsl:apply-templates select="." mode="linkText"/>
      </xsl:attribute>
      <xsl:apply-templates mode="miniMarkupHtml"/>
    </a>
  </xsl:template>

  <xsl:template match="external" mode="miniMarkupText">
      <xsl:apply-templates mode="miniMarkupText"/>
  </xsl:template>

  <xsl:template match="quote" mode="miniMarkupHtml">
    <q>
      <xsl:if test="@lang">
        <xsl:attribute name="lang">
          <xsl:value-of select="@lang"/>
        </xsl:attribute>
      </xsl:if>
      <xsl:apply-templates mode="miniMarkupHtml"/>
    </q>
  </xsl:template>

  <xsl:template match="quote" mode="miniMarkupText">
      <xsl:text>«&#160;</xsl:text>
      <xsl:apply-templates mode="miniMarkupText"/>
      <xsl:text>&#160;»</xsl:text>
  </xsl:template>

  <xsl:template match="no" mode="miniMarkupHtml">
      <xsl:text>n</xsl:text>
      <sup>
        <xsl:text>o</xsl:text>
      </sup>
      <xsl:text>&#160;</xsl:text>
      <xsl:apply-templates mode="miniMarkupHtml"/>
  </xsl:template>

  <xsl:template match="no" mode="miniMarkupText">
      <xsl:text>n°&#160;</xsl:text>
      <xsl:apply-templates mode="miniMarkupText"/>
  </xsl:template>

  <xsl:template match="abbrev" mode="miniMarkupHtml">
    <abbr>
      <xsl:variable name="ref" select="@ref"/>
      <xsl:attribute name="title">
        <xsl:apply-templates
            select="//abbrevs/abbrev[@id=$ref]/long"
            mode="miniMarkupText"/>
      </xsl:attribute>
      <xsl:choose>
        <xsl:when test="*">
          <xsl:apply-templates mode="miniMarkupHtml"/>
        </xsl:when>
        <xsl:otherwise>
          <xsl:apply-templates
              select="//abbrevs/abbrev[@id=$ref]/short"
              mode="miniMarkupHtml"/>
        </xsl:otherwise>
      </xsl:choose>
    </abbr>
  </xsl:template>

  <xsl:template match="abbrev" mode="miniMarkupText">
    <xsl:choose>
      <xsl:when test="*">
        <xsl:apply-templates mode="miniMarkupText"/>
      </xsl:when>
      <xsl:otherwise>
        <xsl:apply-templates
            select="//abbrevs/abbrev[@id=current()/@ref]/short"
            mode="miniMarkupText"/>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>

  <xsl:template match="ordinal" mode="miniMarkupHtml">
    <xsl:value-of select="@num"/>
    <sup>
      <xsl:value-of select="@suffix"/>
    </sup>
  </xsl:template>

  <xsl:template match="ordinal" mode="miniMarkupText">
    <xsl:value-of
        select="concat(@num, if (@suffix = 'o') then '°' else @suffix)"/>
  </xsl:template>

  <xsl:template match="foreign" mode="miniMarkupHtml">
    <span class="foreign" lang="{@lang}">
      <xsl:apply-templates mode="miniMarkupHtml"/>
    </span>
  </xsl:template>

  <xsl:template match="foreign" mode="miniMarkupText">
    <xsl:apply-templates mode="miniMarkupText"/>
  </xsl:template>

  <xsl:template match="items" mode="miniMarkupHtml">
    <ul>
      <xsl:for-each select="item">
        <li>
          <xsl:apply-templates mode="miniMarkupHtml"/>
        </li>
      </xsl:for-each>
    </ul>
  </xsl:template>

  <xsl:template match="items" mode="miniMarkupText">
    <xsl:for-each select="item">
      <xsl:text>•&#160;</xsl:text>
      <xsl:apply-templates mode="miniMarkupText"/>
      <xsl:text>&#xA;</xsl:text>
    </xsl:for-each>
  </xsl:template>

</xsl:stylesheet>
