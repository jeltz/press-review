<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:fn="http://www.w3.org/2005/02/xpath-functions"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    xmlns:t="ffdn:press-review"
    exclude-result-prefixes="xs fn t xsl"
    version="2.0">

  <!--<xsl:strip-space elements="*"/>-->

  <!-- VARIABLES AND PARAMS -->

  <xsl:param name="published.only"/>

  <xsl:variable name="page.base-url"
      select="'https://perso.crans.org/jeltz/revue/'"/>

  <xsl:variable name="id.edition.prefix" select="'edition-'"/>
  <xsl:variable name="id.brief.prefix" select="'brief-'"/>
  <xsl:variable name="id.topic.prefix" select="'topic-'"/>

  <xsl:variable name="source.url"
      select="'https://code.ffdn.org/jeltz/press-review'"/>
  <xsl:variable name="source.title" select="'Source'"/>

  <xsl:variable name="licence.url"
      select="'https://creativecommons.org/licenses/by-sa/4.0/legalcode'"/>
  <xsl:variable name="licence.title" select="'CC BY-SA 4.0'"/>

  <xsl:variable name="atom.header.sources" select="'Sources'"/>
  <xsl:variable name="atom.header.topics" select="'Thèmes'"/>
  <xsl:variable name="atom.iri.prefix"
      select="'brief:b0e78058-47a9-4298-a240-c19f66967eb5'"/>

  <xsl:variable name="header.label.topic" select="'Thème'"/>
  <xsl:variable name="header.label.edition" select="'Édition'"/>

  <xsl:variable name="legal.title" select="'Mentions légales'"/>

  <!-- BASE -->

  <xsl:template name="html.base">
    <xsl:param name="main"/>
    <html lang="{t:get-lang(.)}">
      <head>
        <title>
          <xsl:apply-templates
              select="//t:reviews/t:title"
              mode="markup.inline.text"/>
        </title>
        <link rel="stylesheet" href="base.css"/>
        <link rel="icon" type="image/x-icon" href="icon.svg"/>
        <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
        <meta name="robots" content="noindex"/>
        <link rel="alternate" type="application/atom+xml"
            href="index.atom" title="Atom"/>
      </head>
      <body>
        <header id="top">
          <h1>
            <a href="index.html">
              <xsl:apply-templates
                  select="//t:reviews/t:title"
                  mode="markup.inline.html"/>
            </a>
          </h1>
        </header>
        <main>
          <xsl:copy-of select="$main"/>
        </main>
        <footer>
          <p>
            <xsl:text>Contenu sous licence </xsl:text>
            <a rel="external" href="{$licence.url}">
              <xsl:value-of select="$licence.title"/>
            </a>
            <xsl:text>&#160;·&#160;</xsl:text>
            <a rel="external" href="{$source.url}">
              <xsl:value-of select="$source.title"/>
            </a>
            <xsl:text>&#160;·&#160;</xsl:text>
            <a rel="external" href="legal.html">
              <xsl:value-of select="$legal.title"/>
            </a>
          </p>
        </footer>
        <a id="goto-top" href="#top" role="button">
          <xsl:text>🡅</xsl:text>
        </a>
      </body>
    </html>
  </xsl:template>

  <!-- UTILS -->

  <xsl:function name="t:to-bool" as="xs:boolean">
    <xsl:param name="string" as="xs:string?"/>
    <xsl:param name="default" as="xs:boolean"/>
    <xsl:choose>
      <xsl:when test="string-length($string) > 0">
        <xsl:variable name="lower" select="lower-case($string)"/>
        <xsl:sequence select="$lower = 'true' or $lower = '1'"/>
      </xsl:when>
      <xsl:otherwise>
        <xsl:sequence select="$default"/>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:function>

  <xsl:function name="t:is-published" as="xs:boolean">
    <xsl:param name="published" as="xs:string?"/>
    <xsl:sequence
        select="t:to-bool($published, true())
            or not(t:to-bool($published.only, false()))"/>
  </xsl:function>

  <xsl:function name="t:is-paywalled" as="xs:boolean">
    <xsl:param name="paywall" as="xs:string?"/>
    <xsl:sequence select="t:to-bool($paywall, false())"/>
  </xsl:function>

  <xsl:function name="t:get-lang" as="xs:string">
    <xsl:param name="element"/>
    <xsl:sequence
        select="$element/ancestor-or-self::*[@xml:lang][last()]
                /@xml:lang"/>
  </xsl:function>

  <xsl:function name="t:get-url" as="xs:string">
    <xsl:param name="page" as="xs:string"/>
    <xsl:param name="prefix" as="xs:string"/>
    <xsl:param name="id" as="xs:string"/>
    <xsl:sequence select="concat($page, '#', $prefix, $id)"/>
  </xsl:function>

  <xsl:function name="t:get-edition-url" as="xs:string">
    <xsl:param name="id" as="xs:string"/>
    <xsl:sequence select="t:get-url('index.html', $id.edition.prefix, $id)"/>
  </xsl:function>

  <xsl:function name="t:get-brief-url" as="xs:string">
    <xsl:param name="id" as="xs:string"/>
    <xsl:sequence select="t:get-url('index.html', $id.brief.prefix, $id)"/>
  </xsl:function>

  <xsl:function name="t:get-topic-url" as="xs:string">
    <xsl:param name="id" as="xs:string"/>
    <xsl:sequence select="t:get-url('topics.html', $id.topic.prefix, $id)"/>
  </xsl:function>

  <!-- TITLE -->

  <xsl:template match="t:abbrev" mode="title.short.html">
    <xsl:apply-templates select="t:short" mode="markup.inline.html"/>
  </xsl:template>

  <xsl:template match="t:abbrev" mode="title.long.html">
    <xsl:apply-templates select="t:long" mode="markup.inline.html"/>
  </xsl:template>

  <xsl:template match="t:abbrev" mode="title.long.text">
    <xsl:apply-templates select="t:long" mode="markup.inline.text"/>
  </xsl:template>

  <xsl:template match="t:abbrev" mode="title.full.html">
    <xsl:apply-templates select="t:long" mode="markup.inline.html"/>
    <xsl:text> (</xsl:text>
    <xsl:apply-templates select="t:short" mode="markup.inline.html"/>
    <xsl:text>)</xsl:text>
  </xsl:template>

  <xsl:template match="t:abbrev" mode="title.full.text">
    <xsl:apply-templates select="t:long" mode="markup.inline.text"/>
    <xsl:text> (</xsl:text>
    <xsl:apply-templates select="t:short" mode="markup.inline.text"/>
    <xsl:text>)</xsl:text>
  </xsl:template>

  <xsl:template match="t:topic" mode="title.full.html">
    <xsl:choose>
      <xsl:when test="t:abbrev">
        <xsl:apply-templates
            select="//t:abbrevs/t:abbrev[@id=current()/t:abbrev/@ref]"
            mode="title.full.html"/>
      </xsl:when>
      <xsl:when test="t:short">
        <xsl:apply-templates select="t:title" mode="markup.inline.html"/>
        <xsl:text> (</xsl:text>
        <xsl:apply-templates select="t:short" mode="markup.inline.html"/>
        <xsl:text>)</xsl:text>
      </xsl:when>
      <xsl:otherwise>
        <xsl:apply-templates select="t:title" mode="markup.inline.html"/>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>

  <xsl:template match="t:topic" mode="title.full.text">
    <xsl:choose>
      <xsl:when test="t:abbrev">
        <xsl:apply-templates
            select="//t:abbrevs/t:abbrev[@id=current()/t:abbrev/@ref]"
            mode="title.full.text"/>
      </xsl:when>
      <xsl:when test="t:short">
        <xsl:apply-templates select="t:title" mode="markup.inline.html"/>
        <xsl:text> (</xsl:text>
        <xsl:apply-templates select="t:short" mode="markup.inline.html"/>
        <xsl:text>)</xsl:text>
      </xsl:when>
      <xsl:otherwise>
        <xsl:apply-templates select="t:title" mode="markup.inline.html"/>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>

  <!-- LINK -->

  <xsl:template match="t:topic" mode="link.short.html">
    <a href="{t:get-topic-url(@id)}">
      <xsl:choose>
        <xsl:when test="t:abbrev">
          <xsl:variable name="ref" select="t:abbrev/@ref"/>
          <xsl:attribute name="title">
            <xsl:apply-templates
                select="//t:abbrevs/t:abbrev[@id=$ref]"
                mode="title.long.text"/>
          </xsl:attribute>
          <xsl:apply-templates
              select="//t:abbrevs/t:abbrev[@id=$ref]"
              mode="title.short.html"/>
        </xsl:when>
        <xsl:when test="t:short">
          <xsl:attribute name="title">
            <xsl:apply-templates select="t:title" mode="markup.inline.text"/>
          </xsl:attribute>
          <xsl:apply-templates select="t:short" mode="markup.inline.html"/>
        </xsl:when>
        <xsl:otherwise>
          <xsl:apply-templates select="t:title" mode="markup.inline.html"/>
        </xsl:otherwise>
      </xsl:choose>
    </a>
  </xsl:template>

  <xsl:template match="t:topic" mode="link.long.html">
    <a href="{t:get-topic-url(@id)}">
      <xsl:choose>
        <xsl:when test="t:abbrev">
          <xsl:apply-templates
              select="//t:abbrevs/t:abbrev[@id=current()/t:abbrev/@ref]"
              mode="title.long.html"/>
        </xsl:when>
        <xsl:otherwise>
          <xsl:apply-templates select="t:title" mode="markup.inline.html"/>
        </xsl:otherwise>
      </xsl:choose>
    </a>
  </xsl:template>

  <xsl:template match="t:topic" mode="link.full.html">
    <a href="{t:get-topic-url(@id)}">
      <xsl:apply-templates select="." mode="title.full.html"/>
    </a>
  </xsl:template>

  <xsl:template match="t:edition" mode="link.html">
    <a href="{t:get-edition-url(@id)}">
      <xsl:apply-templates select="t:title" mode="markup.inline.html"/>
    </a>
  </xsl:template>

  <xsl:template match="t:brief" mode="link.html">
    <a href="{t:get-brief-url(@id)}">
      <xsl:apply-templates select="t:title" mode="markup.inline.html"/>
    </a>
  </xsl:template>

  <!-- TIME -->

  <xsl:template match="t:edition" mode="time.html">
    <time datetime="{@date}">
      <xsl:apply-templates select="." mode="time.text"/>
    </time>
  </xsl:template>

  <xsl:template match="t:edition" mode="time.text">
    <xsl:value-of
        select="format-date(@date, '[D01] [Mn] [Y0001]', 'fr', 'AD', 'FR')"/>
  </xsl:template>

  <!-- BLOCK -->

  <xsl:template match="t:para" mode="markup.html">
    <p>
      <xsl:apply-templates mode="markup.inline.html"/>
    </p>
  </xsl:template>

  <xsl:template match="t:para" mode="markup.text">
    <xsl:param name="prefix" as="xs:string?"/>
    <xsl:param name="suffix" as="xs:string?"/>
    <xsl:value-of select="$prefix"/>
    <xsl:apply-templates mode="markup.inline.text"/>
    <xsl:value-of select="$suffix"/>
    <xsl:text>&#xA;</xsl:text>
  </xsl:template>

  <xsl:template match="t:quote" mode="markup.html">
    <blockquote>
      <xsl:if test="@xml:lang">
        <xsl:attribute name="lang">
          <xsl:value-of select="@xml:lang"/>
        </xsl:attribute>
      </xsl:if>
      <xsl:apply-templates mode="markup.html"/>
    </blockquote>
  </xsl:template>

  <xsl:template match="t:quote" mode="markup.text">
    <xsl:for-each select="*">
      <xsl:choose>
        <xsl:when test="position() = last()">
          <xsl:apply-templates select="." mode="markup.text">
            <xsl:with-param name="prefix" select="'  «&#160;'"/>
            <xsl:with-param name="suffix" select="'&#160;»'"/>
          </xsl:apply-templates>
        </xsl:when>
        <xsl:otherwise>
          <xsl:apply-templates select="." mode="markup.text">
            <xsl:with-param name="prefix" select="'  «&#160;'"/>
          </xsl:apply-templates>
        </xsl:otherwise>
      </xsl:choose>
    </xsl:for-each>
  </xsl:template>

  <xsl:template match="t:items" mode="markup.html">
    <ul>
      <xsl:for-each select="t:item">
        <li>
          <xsl:apply-templates mode="markup.inline.html"/>
        </li>
      </xsl:for-each>
    </ul>
  </xsl:template>

  <xsl:template match="t:items" mode="markup.text">
    <xsl:param name="prefix" as="xs:string?"/> 
    <xsl:param name="suffix" as="xs:string?"/>
    <xsl:for-each select="t:item">
      <xsl:value-of select="$prefix"/>
      <xsl:text>  •&#160;</xsl:text>
      <xsl:apply-templates mode="markup.text"/>
      <xsl:if test="position() = last()">
        <xsl:value-of select="$suffix"/>
      </xsl:if>
      <xsl:text>&#xA;</xsl:text>
    </xsl:for-each>
  </xsl:template>

  <!-- INLINE -->

  <xsl:template match="t:quote" mode="markup.inline.html">
    <q>
      <xsl:if test="@xml:lang">
        <xsl:attribute name="lang">
          <xsl:value-of select="@xml:lang"/>
        </xsl:attribute>
      </xsl:if>
      <xsl:apply-templates mode="markup.inline.html"/>
    </q>
  </xsl:template>

  <xsl:template match="t:quote" mode="markup.inline.text">
    <xsl:text>«&#160;</xsl:text>
    <xsl:apply-templates mode="markup.inline.text"/>
    <xsl:text>&#160;»</xsl:text>
  </xsl:template>

  <xsl:template match="t:no" mode="markup.inline.html">
    <xsl:text>n</xsl:text>
    <sup>
      <xsl:text>o</xsl:text>
    </sup>
    <xsl:text>&#160;</xsl:text>
    <xsl:apply-templates mode="markup.inline.html"/>
  </xsl:template>

  <xsl:template match="t:no" mode="markup.inline.text">
    <xsl:text>n°&#160;</xsl:text>
    <xsl:apply-templates mode="markup.inline.text"/>
  </xsl:template>

  <xsl:template match="t:abbrev" mode="markup.inline.html">
    <abbr>
      <xsl:variable name="ref" select="@ref"/>
      <xsl:attribute name="title">
        <xsl:apply-templates
            select="//t:abbrevs/t:abbrev[@id=$ref]/t:long"
            mode="markup.inline.text"/>
      </xsl:attribute>
      <xsl:choose>
        <xsl:when test="t:*">
          <xsl:apply-templates mode="markup.inline.html"/>
        </xsl:when>
        <xsl:otherwise>
          <xsl:apply-templates
              select="//t:abbrevs/t:abbrev[@id=$ref]/t:short"
              mode="markup.inline.html"/>
        </xsl:otherwise>
      </xsl:choose>
    </abbr>
  </xsl:template>

  <xsl:template match="t:abbrev" mode="markup.inline.text">
    <xsl:choose>
      <xsl:when test="t:*">
        <xsl:apply-templates mode="markup.inline.text"/>
      </xsl:when>
      <xsl:otherwise>
        <xsl:apply-templates
            select="//t:abbrevs/t:abbrev[@id=current()/@ref]/t:short"
            mode="markup.inline.text"/>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>

  <xsl:template match="t:ordinal" mode="markup.inline.html">
    <xsl:value-of select="@num"/>
    <sup>
      <xsl:value-of select="@suffix"/>
    </sup>
  </xsl:template>

  <xsl:template match="t:ordinal" mode="markup.inline.text">
    <xsl:value-of
        select="concat(@num, if (@suffix = 'o') then '°' else @suffix)"/>
  </xsl:template>

  <xsl:template match="t:foreign" mode="markup.inline.html">
    <span class="foreign" lang="{@xml:lang}">
      <xsl:apply-templates mode="markup.inline.html"/>
    </span>
  </xsl:template>

  <xsl:template match="t:foreign" mode="markup.inline.text">
    <xsl:apply-templates mode="markup.inline.text"/>
  </xsl:template>

  <xsl:template match="t:emph" mode="markup.inline.html">
    <em>
      <xsl:apply-templates mode="markup.inline.html"/>
    </em>
  </xsl:template>

  <xsl:template match="t:emph" mode="markup.inline.text">
      <xsl:text>**</xsl:text>
      <xsl:apply-templates mode="markup.inline.text"/>
      <xsl:text>**</xsl:text>
  </xsl:template>

  <xsl:template match="t:legifrance" mode="markup.inline.html">
    <a rel="external noreferrer">
      <xsl:attribute name="href">
        <xsl:text>https://www.legifrance.gouv.fr/</xsl:text>
        <xsl:choose>
          <xsl:when test="@loda">
            <xsl:value-of select="concat('loda/id/', @loda, '/')"/>
          </xsl:when>
          <xsl:when test="@cnil">
            <xsl:value-of select="concat('cnil/id/', @cnil, '/')"/>
          </xsl:when>
          <xsl:when test="@article">
            <xsl:value-of
                select="concat('codes/article_lc/', @article, '/')"/>
          </xsl:when>
          <xsl:when test="@jorf">
            <xsl:value-of select="concat('jorf/id/', @jorf, '/')"/>
          </xsl:when>
        </xsl:choose>
        <xsl:if test="@date">
          <xsl:value-of select="concat(@date, '/')"/>
        </xsl:if>
      </xsl:attribute>
      <xsl:apply-templates mode="markup.inline.html"/>
    </a>
  </xsl:template>

  <xsl:template match="t:conseil-etat" mode="markup.inline.html">
    <a rel="external noreferrer"
        href="{concat('https://www.conseil-etat.fr/fr/arianeweb/CE/decision/',
                      @date, '/', @case)}">
      <xsl:apply-templates mode="markup.inline.html"/>
    </a>
  </xsl:template>

  <xsl:template match="t:eur-lex" mode="markup.inline.html">
    <a rel="external noreferrer"
        href="{concat('https://eur-lex.europa.eu/legal-content/',
                      upper-case(t:get-lang(.)), '/TXT/?uri=', @uri)}">
      <xsl:apply-templates mode="markup.inline.html"/>
    </a>
  </xsl:template>

  <xsl:template match="t:email" mode="markup.inline.html">
    <a rel="external noreferrer" href="{concat('mailto:', @address)}">
      <xsl:apply-templates mode="markup.inline.html"/>
    </a>
  </xsl:template>

  <xsl:template match="t:external" mode="markup.inline.html">
    <a rel="external noreferrer" href="{@href}">
      <xsl:if test="t:is-paywalled(@paywall)">
        <xsl:attribute name="class" select="'paywall'"/>
      </xsl:if>
      <xsl:apply-templates mode="markup.inline.html"/>
    </a>
  </xsl:template>

  <xsl:template match="t:wikipedia" mode="markup.inline.html">
    <a rel="external noreferrer"
        href="{concat('https://', t:get-lang(.), '.wikipedia.org/wiki/',
                      @title)}">
      <xsl:choose>
        <xsl:when test="t:*|text()">
          <xsl:apply-templates mode="markup.inline.html"/>
        </xsl:when>
        <xsl:when test="@xml:lang">
          <xsl:value-of
              select="concat('Wikipédia (', upper-case(t:get-lang(.)), ')')"/>
        </xsl:when>
        <xsl:otherwise>
          <xsl:text>Wikipédia</xsl:text>
        </xsl:otherwise>
      </xsl:choose>
    </a>
  </xsl:template>

  <xsl:template match="t:curia" mode="markup.inline.html">
    <a rel="external noreferrer"
        href="{concat('https://curia.europa.eu/juris/liste.jsf?language=',
                      lower-case(t:get-lang(.)), '&amp;num=', @case)}">
      <xsl:choose>
        <xsl:when test="t:*|text()">
          <xsl:apply-templates mode="markup.inline.html"/>
        </xsl:when>
        <xsl:otherwise>
          <xsl:value-of select="upper-case(@case)"/>
        </xsl:otherwise>
      </xsl:choose>
    </a>
  </xsl:template>

  <xsl:template
      match="t:external|t:eur-lex|t:legifrance|t:wikipedia
             |t:email|t:conseil-etat|t:curia"
      mode="markup.inline.text">
    <xsl:apply-templates mode="markup.inline.text"/>
  </xsl:template>

  <xsl:template match="t:person" mode="markup.inline.html">
    <xsl:if test="@prefix">
      <xsl:choose>
        <xsl:when test="@prefix = ('Pr', 'Dr', 'Mme', 'Mmes', 'Me')">
          <xsl:value-of select="substring(@prefix, 1, 1)"/>
          <sup>
            <xsl:value-of select="substring(@prefix, 2)"/>
          </sup>
        </xsl:when>
        <xsl:otherwise>
          <xsl:value-of select="@prefix"/>
        </xsl:otherwise>
      </xsl:choose>
      <xsl:text>&#160;</xsl:text>
    </xsl:if>
    <xsl:apply-templates mode="markup.inline.html"/>
  </xsl:template>

  <xsl:template match="t:person" mode="markup.inline.text">
    <xsl:if test="@prefix">
      <xsl:value-of select="concat(@prefix, '&#160;')"/>
    </xsl:if>
    <xsl:apply-templates mode="markup.inline.text"/>
  </xsl:template>

  <xsl:template match="t:todo" mode="markup.inline.html">
    <span class="todo">
      <xsl:choose>
        <xsl:when test="t:*|text()">
          <xsl:apply-templates mode="markup.inline.html"/>
        </xsl:when>
        <xsl:otherwise>
          <xsl:text>⋯</xsl:text>
        </xsl:otherwise>
      </xsl:choose>
    </span>
  </xsl:template>

  <xsl:template match="t:todo" mode="markup.inline.text">
    <span class="todo">
      <xsl:choose>
        <xsl:when test="t:*|text()">
          <xsl:apply-templates mode="markup.inline.text"/>
        </xsl:when>
        <xsl:otherwise>
          <xsl:text>[…]</xsl:text>
        </xsl:otherwise>
      </xsl:choose>
    </span>
  </xsl:template>

  <xsl:template match="t:ins" mode="markup.inline.html">
    <ins>
      <xsl:text>[</xsl:text>
      <xsl:apply-templates mode="markup.inline.html"/>
      <xsl:text>]</xsl:text>
    </ins>
  </xsl:template>

  <xsl:template match="t:ins" mode="markup.inline.text">
    <xsl:text>[</xsl:text>
    <xsl:apply-templates mode="markup.inline.text"/>
    <xsl:text>]</xsl:text>
  </xsl:template>

</xsl:stylesheet>
