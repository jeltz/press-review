<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:t="ffdn:press-review"
    exclude-result-prefixes="t"
    version="2.0">

  <xsl:output method="html" doctype-system="about:legacy-compat"
      indent="yes" encoding="UTF-8"/>

  <xsl:include href="common.xsl"/>

  <xsl:template match="t:reviews">
    <xsl:call-template name="html.base">
      <xsl:with-param name="main">
        <xsl:apply-templates select="t:legal-notice"/>
      </xsl:with-param>
    </xsl:call-template>
  </xsl:template>

  <xsl:template match="t:legal-notice">
    <section class="static">
      <header>
        <h2>
          <xsl:value-of select="$legal.title"/>
        </h2>
      </header>
      <article>
        <xsl:apply-templates select="." mode="markup.html"/>
      </article>
    </section>
  </xsl:template>

</xsl:stylesheet>
