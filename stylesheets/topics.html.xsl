<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:t="ffdn:press-review"
    exclude-result-prefixes="t"
    version="2.0">

  <xsl:output method="html" doctype-system="about:legacy-compat"
      indent="yes" encoding="UTF-8"/>

  <xsl:include href="common.xsl"/>

  <xsl:template match="t:reviews">
    <xsl:call-template name="html.base">
      <xsl:with-param name="main">
        <xsl:apply-templates select="t:topics"/>
      </xsl:with-param>
    </xsl:call-template>
  </xsl:template>

  <xsl:template match="t:topics">
    <xsl:for-each select="t:topic">
      <xsl:apply-templates select="."/>
    </xsl:for-each>
  </xsl:template>

  <xsl:template match="t:topic">
    <xsl:variable name="id" select="@id"/>
    <xsl:if test="//t:editions/t:edition[t:is-published(@published)]
                  /t:brief/t:topic[@ref=$id]">
      <section class="topic" id="{concat($id.topic.prefix, @id)}">
        <header>
          <h2>
            <span class="header-label">
              <xsl:value-of select="$header.label.topic"/>
            </span>
            <xsl:apply-templates select="." mode="link.long.html"/>
          </h2>
        </header>
        <xsl:apply-templates select="t:description/t:*" mode="markup.html"/>
        <xsl:if test="t:sources/*">
          <ul class="sources">
            <xsl:for-each select="t:sources/t:*">
              <li>
                <xsl:apply-templates select="." mode="markup.inline.html"/>
              </li>
            </xsl:for-each>
          </ul>
        </xsl:if>
        <ul class="related-articles">
          <xsl:for-each
              select="//t:editions/t:edition/t:brief/t:topic[@ref=$id]/..">
            <xsl:sort select="../@date" order="descending" />
            <xsl:if test="t:is-published(../@published)">
              <li>
                <xsl:apply-templates select=".." mode="time.html"/>
                <xsl:apply-templates select="." mode="link.html"/>
              </li>
            </xsl:if>
          </xsl:for-each>
        </ul>
      </section>
    </xsl:if>
  </xsl:template>

</xsl:stylesheet>
