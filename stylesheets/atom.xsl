<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:fn="http://www.w3.org/2005/02/xpath-functions"
    xmlns="http://www.w3.org/2005/Atom"
    version="2.0">

  <xsl:output indent="yes" method="xml" encoding="UTF-8"/>

  <xsl:include href="common.xsl"/>

  <xsl:template match="reviews">
    <feed>
      <title>
        <xsl:value-of select="$pageTitle"/>
      </title>
      <updated>
        <xsl:value-of select="current-dateTime()"/>
      </updated>
      <link href="{$htmlUrl}"/>
      <icon>
        <xsl:value-of select="concat($htmlUrl, 'icon.svg')"/>
      </icon>
      <id>
        <xsl:value-of select="$atomIriPrefix"/>
      </id>
      <xsl:for-each select="editions/edition">
        <xsl:sort select="date" order="ascending"/>
        <xsl:if test="fn:isPublished(@published)">
          <xsl:for-each select="brief">
            <xsl:apply-templates select="."/>
          </xsl:for-each>
        </xsl:if>
      </xsl:for-each>
    </feed>
  </xsl:template>

  <xsl:template match="abbrev" mode="fullTitle">
    <xsl:apply-templates select="long" mode="miniMarkupHtml"/>
    <xsl:text> (</xsl:text>
    <xsl:apply-templates select="short" mode="miniMarkupHtml"/>
    <xsl:text>)</xsl:text>
  </xsl:template>

  <xsl:template match="abbrev" mode="fullTitleText">
    <xsl:apply-templates select="long" mode="miniMarkupText"/>
    <xsl:text> (</xsl:text>
    <xsl:apply-templates select="short" mode="miniMarkupText"/>
    <xsl:text>)</xsl:text>
  </xsl:template>

  <xsl:template match="topic" mode="fullTitle">
    <xsl:choose>
      <xsl:when test="abbrev">
        <xsl:apply-templates
            select="//abbrevs/abbrev[@id=current()/abbrev/@ref]"
            mode="fullTitle"/>
      </xsl:when>
      <xsl:when test="boolean(short)">
        <xsl:apply-templates select="title" mode="miniMarkupHtml"/>
        <xsl:text> (</xsl:text>
        <xsl:apply-templates select="short" mode="miniMarkupHtml"/>
        <xsl:text>)</xsl:text>
      </xsl:when>
      <xsl:otherwise>
        <xsl:apply-templates select="title" mode="miniMarkupHtml"/>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>

  <xsl:template match="topic" mode="fullTitleText">
    <xsl:choose>
      <xsl:when test="abbrev">
        <xsl:apply-templates
            select="//abbrevs/abbrev[@id=current()/abbrev/@ref]"
            mode="fullTitleText"/>
      </xsl:when>
      <xsl:when test="boolean(short)">
        <xsl:apply-templates select="title" mode="miniMarkupText"/>
        <xsl:text> (</xsl:text>
        <xsl:apply-templates select="short" mode="miniMarkupText"/>
        <xsl:text>)</xsl:text>
      </xsl:when>
      <xsl:otherwise>
        <xsl:apply-templates select="title" mode="miniMarkupText"/>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>

  <xsl:template match="topic" mode="fullTitleLink">
    <a href="{concat($htmlUrl, '#', $topicLinkPrefix, @id)}">
      <xsl:apply-templates select="." mode="fullTitle"/>
    </a>
  </xsl:template>

  <xsl:template match="source" mode="link">
    <a>
      <xsl:attribute name="href">
        <xsl:apply-templates select="." mode="linkText"/>
      </xsl:attribute>
      <xsl:apply-templates select="title" mode="miniMarkupHtml"/>
    </a>
  </xsl:template>

  <xsl:template match="brief">
    <entry>
      <title>
        <xsl:if test="fn:toBool(@wip, false())">
          <xsl:text>[WIP] </xsl:text>
        </xsl:if>
        <xsl:apply-templates select="title" mode="miniMarkupText"/>
      </title>
      <id>
        <xsl:value-of select="concat($atomIriPrefix, ':', @id)"/>
      </id>
      <published>
        <xsl:value-of select="../@date"/>
      </published>
      <xsl:for-each select="topic">
        <category>
          <xsl:attribute name="term">
            <xsl:apply-templates
                select="//topics/topic[@id=current()/@ref]"
                mode="fullTitleText"/>
          </xsl:attribute>
        </category>
      </xsl:for-each>
      <link href="{concat($htmlUrl, '#', $briefLinkPrefix, @id)}"/>
      <content>
        <xsl:apply-templates select="content/*" mode="miniMarkupHtml"/>
        <xsl:if test="boolean(source)">
          <h1>
            <xsl:value-of select="$sourcesAtomHeader"/>
          </h1>
          <ul>
            <xsl:for-each select="source">
              <li>
                <xsl:apply-templates select="." mode="link"/>
              </li>
            </xsl:for-each>
          </ul>
        </xsl:if>
        <xsl:if test="boolean(topic)">
          <h1>
            <xsl:value-of select="$topicsAtomHeader"/>
          </h1>
          <ul>
            <xsl:for-each select="topic">
              <li>
                <xsl:apply-templates
                    select="//topics/topic[@id=current()/@ref]"
                    mode="fullTitleLink"/>
              </li>
            </xsl:for-each>
          </ul>
        </xsl:if>
      </content>
    </entry>
  </xsl:template>

</xsl:stylesheet>
