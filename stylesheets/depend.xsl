<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xi="http://www.w3.org/2001/XInclude"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    xmlns:fn="http://www.w3.org/2005/02/xpath-functions"
    version="2.0">

  <xsl:output method="text" indent="no"/>

  <xsl:param name="separator" select="' '"/>
  <xsl:param name="target"/>

  <!-- From https://stackoverflow.com/questions/3116942 -->

  <xsl:function name="fn:dirname" as="xs:string">
    <xsl:param name="path" as="xs:string"></xsl:param>
    <xsl:sequence
        select="string-join(
            tokenize($path, '/')[position() != last()], '/')"/>
  </xsl:function>

  <xsl:function name="fn:combinePaths" as="xs:string">
    <xsl:param name="path" as="xs:string"></xsl:param>
    <xsl:param name="current" as="xs:string"></xsl:param>
    <xsl:choose>
      <xsl:when test="not(starts-with($path, '/'))">
        <xsl:sequence select="concat($current, $path)"/>
      </xsl:when>
      <xsl:otherwise>
        <xsl:sequence select="$path"/>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:function>

  <!-- From xmldepend.xsl by Paul DuBois -->

  <xsl:template match="*" mode="include">
    <xsl:param name="directory" as="xs:string"/>
    <xsl:apply-templates select="*" mode="include">
      <xsl:with-param name="directory" select="$directory"/>
    </xsl:apply-templates>
  </xsl:template>

  <xsl:template match="xi:include|xsl:import|xsl:include" mode="include">
    <xsl:param name="directory" as="xs:string"/>
    <xsl:variable name="path" select="fn:combinePaths(@href, $directory)"/>
    <xsl:value-of select="concat($separator, $path)"/>
    <xsl:if test="not(@parse = 'text')">
      <xsl:for-each select="document(@href)">
        <xsl:apply-templates mode="include">
          <xsl:with-param name="directory" select="fn:dirname($path)"/>
        </xsl:apply-templates>
      </xsl:for-each>
    </xsl:if>
  </xsl:template>

  <xsl:template match="/">
    <xsl:value-of select="concat($target, ':')"/>
    <xsl:apply-templates select="." mode="include">
      <xsl:with-param name="directory" select="''"/>
    </xsl:apply-templates>
    <xsl:text>&#10;</xsl:text>
  </xsl:template>

</xsl:stylesheet>
