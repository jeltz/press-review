<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:t="ffdn:press-review"
    exclude-result-prefixes="t"
    version="2.0">

  <xsl:output method="html" doctype-system="about:legacy-compat"
      indent="yes" encoding="UTF-8"/>

  <xsl:include href="common.xsl"/>

  <xsl:template match="t:reviews">
    <xsl:call-template name="html.base">
      <xsl:with-param name="main">
        <xsl:apply-templates select="t:editions"/>
      </xsl:with-param>
    </xsl:call-template>
  </xsl:template>

  <xsl:template match="t:editions">
    <xsl:for-each select="t:edition">
      <xsl:sort select="@date" order="descending"/>
      <xsl:if test="t:is-published(@published)">
        <xsl:apply-templates select="."/>
      </xsl:if>
    </xsl:for-each>
  </xsl:template>

  <xsl:template match="t:edition">
    <section class="edition" id="{concat($id.edition.prefix, @id)}">
      <header>
        <h2>
          <span class="header-label">
            <xsl:value-of select="$header.label.edition"/>
          </span>
          <xsl:apply-templates select="." mode="link.html"/>
        </h2>
        <xsl:apply-templates select="." mode="time.html"/>
      </header>
      <xsl:for-each select="t:brief">
        <xsl:apply-templates select="."/>
      </xsl:for-each>
    </section>
  </xsl:template>

  <xsl:template match="t:brief">
    <article id="{concat($id.brief.prefix, @id)}">
      <header>
        <h3>
          <xsl:apply-templates select="." mode="link.html"/>
        </h3>
      </header>
      <xsl:apply-templates select="t:content/t:*" mode="markup.html"/>
      <footer class="links">
        <ul class="sources">
          <xsl:for-each select="t:sources/t:*">
            <li>
              <xsl:apply-templates select="." mode="markup.inline.html"/>
            </li>
          </xsl:for-each>
        </ul>
        <ul class="topics">
          <xsl:for-each select="t:topic">
            <xsl:sort select="@ref" order="ascending"/>
            <xsl:if test="//t:topics/t:topic[@id=current()/@ref]">
              <li>
                <xsl:apply-templates
                    select="//t:topics/t:topic[@id=current()/@ref]"
                    mode="link.short.html"/>
              </li>
            </xsl:if>
          </xsl:for-each>
        </ul>
      </footer>
    </article>
  </xsl:template>

</xsl:stylesheet>
