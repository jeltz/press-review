<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:fn="http://www.w3.org/2005/02/xpath-functions"
    version="2.0">

  <xsl:output
      indent="yes"
      method="html"
      doctype-system="about:legacy-compat"
      encoding="UTF-8"/>

  <xsl:include href="common.xsl"/>

  <xsl:template match="reviews">
    <html lang="fr">
      <head>
        <title><xsl:value-of select="$pageTitle"/></title>
        <link rel="stylesheet" href="base.css"/>
        <link rel="icon" type="image/x-icon" href="icon.svg"/>
        <meta name="robots" content="noindex"/>
        <link
            rel="alternate"
            type="application/atom+xml"
            href="index.atom"
            title="Atom"/>
      </head>
      <body>
        <header>
          <h1><xsl:value-of select="$pageTitle"/></h1>
        </header>
        <main>
          <xsl:for-each select="editions/edition">
            <xsl:sort select="@date" order="descending"/>
            <xsl:if test="fn:isPublished(@published)">
              <xsl:apply-templates select="."/>
            </xsl:if>
          </xsl:for-each>
        </main>
        <xsl:for-each select="topics/topic">
          <xsl:apply-templates select="."/>
        </xsl:for-each>
        <footer>
          <p>
            <xsl:text>Contenu sous licence </xsl:text>
            <a rel="external" href="{$licenceUrl}">
              <xsl:value-of select="$licenceName"/>
            </a>
            <xsl:text>&#160;·&#160;</xsl:text>
            <a rel="external" href="{$sourceUrl}">
              <xsl:text>Source</xsl:text>
            </a>
          </p>
        </footer>
      </body>
    </html>
  </xsl:template>

  <xsl:template match="abbrev" mode="shortTitle">
    <xsl:apply-templates select="short" mode="miniMarkupHtml"/>
  </xsl:template>

  <xsl:template match="abbrev" mode="longTitle">
    <xsl:apply-templates select="long" mode="miniMarkupHtml"/>
  </xsl:template>

  <xsl:template match="abbrev" mode="longTitleText">
    <xsl:apply-templates select="long" mode="miniMarkupText"/>
  </xsl:template>

  <xsl:template match="topic" mode="longLink">
    <a href="{concat('#', $topicLinkPrefix, @id)}">
      <xsl:choose>
        <xsl:when test="abbrev">
          <xsl:apply-templates
              select="//abbrevs/abbrev[@id=current()/abbrev/@ref]"
              mode="longTitle"/>
        </xsl:when>
        <xsl:otherwise>
          <xsl:apply-templates select="title" mode="miniMarkupHtml"/>
        </xsl:otherwise>
      </xsl:choose>
    </a>
  </xsl:template>

  <xsl:template match="topic" mode="shortLink">
    <a href="{concat('#', $topicLinkPrefix, @id)}">
      <xsl:choose>
        <xsl:when test="abbrev">
          <xsl:variable name="ref" select="abbrev/@ref"/>
          <xsl:attribute name="title">
            <xsl:apply-templates
                select="//abbrevs/abbrev[@id=$ref]"
                mode="longTitleText"/>
          </xsl:attribute>
          <xsl:apply-templates
              select="//abbrevs/abbrev[@id=$ref]"
              mode="shortTitle"/>
        </xsl:when>
        <xsl:when test="boolean(short)">
          <xsl:attribute name="title">
            <xsl:apply-templates select="title" mode="miniMarkupHtml"/>
          </xsl:attribute>
          <xsl:apply-templates select="short" mode="miniMarkupHtml"/>
        </xsl:when>
        <xsl:otherwise>
          <xsl:apply-templates select="title" mode="miniMarkupHtml"/>
        </xsl:otherwise>
      </xsl:choose>
    </a>
  </xsl:template>

  <xsl:template match="topic">
    <xsl:variable name="id" select="@id"/>
    <xsl:if test="boolean(
        //editions/edition[fn:isPublished(@published)]/brief/topic[@ref=$id])">
      <section class="topic" id="{concat($topicLinkPrefix, @id)}">
        <header>
          <h2>
            <span class="header-label">
              <xsl:value-of select="$topicLabel"/>
            </span>
            <xsl:apply-templates select="." mode="longLink"/>
          </h2>
        </header>
        <xsl:apply-templates select="description/*" mode="miniMarkupHtml"/>
        <xsl:if test="boolean(source)">
          <ul class="sources">
            <xsl:for-each select="source">
              <li>
                <xsl:apply-templates select="." mode="link"/>
              </li>
            </xsl:for-each>
          </ul>
        </xsl:if>
        <ul class="related-articles">
          <xsl:for-each select="//editions/edition/brief/topic[@ref=$id]/..">
            <xsl:sort select="../@date" order="descending" />
            <xsl:if test="fn:isPublished(../@published)">
              <li>
                <xsl:apply-templates select=".." mode="time"/>
                <xsl:apply-templates select="." mode="link"/>
              </li>
            </xsl:if>
          </xsl:for-each>
        </ul>
      </section>
    </xsl:if>
  </xsl:template>

  <xsl:template match="source" mode="link">
    <a rel="noreferrer">
      <xsl:if test="fn:isPaywalled(@paywall)">
        <xsl:attribute name="class">
          <xsl:text>paywall</xsl:text>
        </xsl:attribute>
      </xsl:if>
      <xsl:attribute name="href">
        <xsl:apply-templates select="." mode="linkText"/>
      </xsl:attribute>
      <xsl:apply-templates select="title" mode="miniMarkupHtml"/>
    </a>
  </xsl:template>

  <xsl:template match="edition" mode="link">
    <a href="{concat('#', $editionLinkPrefix, @id)}">
      <xsl:apply-templates select="title" mode="miniMarkupHtml"/>
    </a>
  </xsl:template>

  <xsl:template match="edition" mode="time">
    <time datetime="{@date}">
      <xsl:apply-templates select="." mode="timeText"/>
    </time>
  </xsl:template>

  <xsl:template match="edition">
    <section class="edition" id="{concat($editionLinkPrefix, @id)}">
      <header>
        <h2>
          <span class="header-label">
            <xsl:value-of select="$editionLabel"/>
          </span>
          <xsl:apply-templates select="." mode="link"/>
        </h2>
        <xsl:apply-templates select="." mode="time"/>
      </header>
      <xsl:for-each select="brief">
        <xsl:apply-templates select="."/>
      </xsl:for-each>
    </section>
  </xsl:template>

  <xsl:template match="brief" mode="link">
    <a href="{concat('#', $briefLinkPrefix, @id)}">
      <xsl:apply-templates select="title" mode="miniMarkupHtml"/>
    </a>
  </xsl:template>

  <xsl:template match="brief">
    <article id="{concat($briefLinkPrefix, @id)}">
      <header>
        <h3>
          <xsl:if test="fn:toBool(@wip, false())">
            <xsl:text>[WIP] </xsl:text>
          </xsl:if>
          <xsl:apply-templates select="." mode="link"/>
        </h3>
      </header>
      <xsl:apply-templates select="content/*" mode="miniMarkupHtml"/>
      <footer class="links">
        <ul class="sources">
          <xsl:for-each select="source">
            <li>
              <xsl:apply-templates select="." mode="link"/>
            </li>
          </xsl:for-each>
        </ul>
        <ul class="topics">
          <xsl:for-each select="topic">
            <xsl:sort select="@ref" order="ascending"/>
            <xsl:if test="boolean(//topics/topic[@id=current()/@ref])">
              <li>
                <xsl:apply-templates
                    select="//topics/topic[@id=current()/@ref]"
                    mode="shortLink"/>
              </li>
            </xsl:if>
          </xsl:for-each>
        </ul>
      </footer>
    </article>
  </xsl:template>

</xsl:stylesheet>
