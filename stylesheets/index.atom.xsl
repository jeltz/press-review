<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:t="ffdn:press-review"
    xmlns="http://www.w3.org/2005/Atom"
    exclude-result-prefixes="t"
    version="2.0">

  <xsl:output method="xml" indent="yes" encoding="UTF-8"/>

  <xsl:include href="common.xsl"/>

  <xsl:template match="t:reviews">
    <feed>
      <title>
        <xsl:apply-templates
            select="//t:reviews/t:title"
            mode="markup.inline.text"/>
      </title>
      <updated>
        <xsl:value-of select="current-dateTime()"/>
      </updated>
      <link href="{$page.base-url}"/>
      <icon>
        <xsl:value-of select="concat($page.base-url, 'icon.svg')"/>
      </icon>
      <id>
        <xsl:value-of select="$atom.iri.prefix"/>
      </id>
      <xsl:for-each select="t:editions/t:edition">
        <xsl:sort select="@date" order="ascending"/>
        <xsl:if test="t:is-published(@published)">
          <xsl:for-each select="t:brief">
            <xsl:apply-templates select="."/>
          </xsl:for-each>
        </xsl:if>
      </xsl:for-each>
    </feed>
  </xsl:template>

  <xsl:template match="t:brief">
    <entry>
      <title>
        <xsl:apply-templates select="t:title" mode="markup.inline.text"/>
      </title>
      <id>
        <xsl:value-of select="concat($atom.iri.prefix, ':', @id)"/>
      </id>
      <published>
        <xsl:value-of select="../@date"/>
      </published>
      <xsl:for-each select="t:topic">
        <category>
          <xsl:attribute name="term">
            <xsl:apply-templates
                select="//t:topics/t:topic[@id=current()/@ref]"
                mode="title.full.text"/>
          </xsl:attribute>
        </category>
      </xsl:for-each>
      <link href="{t:get-brief-url(@id)}"/>
      <content>
        <xsl:apply-templates select="t:content/t:*" mode="markup.html"/>
        <xsl:if test="t:sources">
          <h1>
            <xsl:value-of select="$atom.header.sources"/>
          </h1>
          <ul>
            <xsl:for-each select="t:sources/t:*">
              <li>
                <xsl:apply-templates select="." mode="markup.inline.html"/>
              </li>
            </xsl:for-each>
          </ul>
        </xsl:if>
        <xsl:if test="t:topic">
          <h1>
            <xsl:value-of select="$atom.header.topics"/>
          </h1>
          <ul>
            <xsl:for-each select="t:topic">
              <li>
                <xsl:apply-templates
                    select="//t:topics/t:topic[@id=current()/@ref]"
                    mode="link.full.html"/>
              </li>
            </xsl:for-each>
          </ul>
        </xsl:if>
      </content>
    </entry>
  </xsl:template>

</xsl:stylesheet>
