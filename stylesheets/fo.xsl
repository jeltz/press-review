<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:fo="http://www.w3.org/1999/XSL/Format"
  version="2.0">
  <xsl:output 
    indent="yes"
    method="xml" 
    encoding="UTF-8"/>
  <xsl:strip-space elements="*"/>
  <xsl:template match="reviews">
    <fo:root font-size="11pt" font-family="Open Sans" line-height="1.25">
      <fo:layout-master-set>
        <fo:simple-page-master
          master-name="A4"
          page-width="210mm"
          page-height="297mm"
          margin="1cm">
          <fo:region-body margin="2cm"/>
          <fo:region-before extent="1cm"/>
          <fo:region-after extent="1cm"/>
        </fo:simple-page-master>
      </fo:layout-master-set>
      <xsl:for-each select="editions/edition">
        <xsl:sort select="date" order="descending" />
        <xsl:apply-templates select="."/>
      </xsl:for-each>
        <!--<xsl:for-each select="topics/topic">
          <xsl:apply-templates select="."/>
        </xsl:for-each>-->
        <!--<p>Contenu sous licence <a href="https://creativecommons.org/licenses/by-sa/4.0/legalcode">CC BY-SA 4.0</a>&#160;·&#160;<a href="https://gitlab.crans.org/jeltz/press-review">Source</a></p>-->
    </fo:root>
  </xsl:template>
  <!--<xsl:template match="topic">
    <xsl:variable name="id" select="@id"/>
    <xsl:if test="boolean(//editions/edition/brief/topic[@ref=$id])">
      <section class="topic">
        <xsl:attribute name="id">topic-<xsl:value-of select="@id"/></xsl:attribute>
        <header>
          <h2>
            <span class="header-label">Thème</span>
            <a>
              <xsl:attribute name="href">#topic-<xsl:value-of select="@id"/></xsl:attribute>
              <xsl:choose>
                <xsl:when test="boolean(long)">
                  <xsl:value-of select="long"/>
                </xsl:when>
                <xsl:otherwise>
                  <xsl:value-of select="title"/>
                </xsl:otherwise>
              </xsl:choose>
            </a>
          </h2>
        </header>
        <xsl:apply-templates select="description/*"/>
        <xsl:if test="boolean(source)">
          <ul class="sources">
            <xsl:for-each select="source">
              <li>
                <a>
                  <xsl:attribute name="href"><xsl:value-of select="@href"/></xsl:attribute>
                  <xsl:if test=".[@paywall='true']">
                    <xsl:attribute name="class">paywall</xsl:attribute>
                  </xsl:if>
                  <xsl:attribute name="href"><xsl:value-of select="@href"/></xsl:attribute>
                  <xsl:value-of select="@name"/>
                </a>
              </li>
            </xsl:for-each>
          </ul>
        </xsl:if>
        <ul class="related-articles">
          <xsl:for-each select="//editions/edition/brief/topic[@ref=$id]/..">
            <xsl:sort select="../date" order="descending" />
            <li>
              <time>
                <xsl:attribute name="datetime">
                  <xsl:value-of select="../date"/>
                </xsl:attribute>
                <xsl:value-of select="format-date(../date, '[D01] [Mn] [Y0001]', 'fr', 'AD', 'FR')"/>
              </time>
              <a>
                <xsl:attribute name="href">#brief-<xsl:value-of select="@id"/></xsl:attribute>
                <xsl:value-of select="title"/>
              </a>
            </li>
          </xsl:for-each>
        </ul>
      </section>
    </xsl:if>
  </xsl:template>-->
  <xsl:template match="edition">
    <fo:page-sequence master-reference="A4">
      <!--<fo:static-content flow-name="xsl-region-before">
        <fo:block text-align="center">XXX</fo:block>
      </fo:static-content>-->
      <fo:static-content flow-name="xsl-region-after">
        <fo:block text-align="center" font-size="85%"><fo:page-number/></fo:block>
      </fo:static-content>
      <fo:flow flow-name="xsl-region-body">
        <fo:block space-after="10mm">
          <xsl:attribute name="id">edition-<xsl:value-of select="@id"/></xsl:attribute>
          <fo:block font-size="130%">
            <fo:block font-weight="bold">
              <fo:inline padding-right="10px" color="#729fcf">Édition</fo:inline>
              <xsl:value-of select="title"/>
            </fo:block>
            <fo:block>
              <xsl:value-of select="format-date(date, '[D01] [Mn] [Y0001]', 'fr', 'AD', 'FR')"/>
            </fo:block>
          </fo:block>
          <xsl:for-each select="brief">
            <xsl:apply-templates select="."/>
          </xsl:for-each>
        </fo:block>
      </fo:flow>
    </fo:page-sequence>
  </xsl:template>
  <xsl:template match="brief">
    <fo:block space-after="5mm" space-before="5mm">
      <xsl:attribute name="id">brief-<xsl:value-of select="@id"/></xsl:attribute>
      <fo:block font-weight="bold" font-size="110%" space-after="2.5mm">
        <xsl:value-of select="title"/>
      </fo:block>
      <xsl:apply-templates select="content/*"/>
      <!--
      <div class="links">
        <ul class="sources">
          <xsl:for-each select="source">
            <li>
              <a>
                <xsl:attribute name="href"><xsl:value-of select="@href"/></xsl:attribute>
                <xsl:if test=".[@paywall='true']">
                  <xsl:attribute name="class">paywall</xsl:attribute>
                </xsl:if>
                <xsl:value-of select="@name"/>
              </a>
            </li>
          </xsl:for-each>
        </ul>
        <ul class="topics">
          <xsl:for-each select="topic">
            <xsl:sort select="@ref" order="ascending"/>
            <xsl:variable name="ref" select="@ref"/>
            <li>
              <a>
                <xsl:attribute name="href">#topic-<xsl:value-of select="@ref"/></xsl:attribute>
                <xsl:if test="boolean(//topics/topic[@id=$ref]/long)">
                  <xsl:attribute name="title">
                    <xsl:value-of select="//topics/topic[@id=$ref]/long"/>
                  </xsl:attribute>
                </xsl:if>
                <xsl:value-of select="//topics/topic[@id=$ref]/title"/>
              </a>
            </li>
          </xsl:for-each>
        </ul>
      </div>
      -->
    </fo:block>
  </xsl:template>
  <xsl:template match="para">
    <fo:block space-after="2.5mm"><xsl:apply-templates/></fo:block>
  </xsl:template>
  <xsl:template match="emph">
    <fo:inline font-weight="bold"><xsl:apply-templates/></fo:inline>
  </xsl:template>
  <!--
  <xsl:template match="email">
    <a class="email-address">
      <xsl:attribute name="href">mailto:<xsl:value-of select="text()"/></xsl:attribute>
      <xsl:value-of select="text()"/>
    </a>
  </xsl:template>
  -->
  <xsl:template match="external">
    <fo:basic-link color="#4A6887">
      <xsl:attribute name="external-destination"><xsl:value-of select="@href"/></xsl:attribute>
      <xsl:apply-templates/>
    </fo:basic-link>
    <fo:footnote>
      <fo:inline font-size=".60em" baseline-shift="40%">1</fo:inline>
      <fo:footnote-body font-size="80%">
        <fo:block>
          <fo:inline>1.&#160;</fo:inline>
          <fo:basic-link color="#4A6887">
            <xsl:attribute name="external-destination"><xsl:value-of select="@href"/></xsl:attribute>
            <xsl:value-of select="@href"/>
          </fo:basic-link>
        </fo:block>
      </fo:footnote-body>
    </fo:footnote>
  </xsl:template>
  <xsl:template match="quote">
    <xsl:text>«&#160;</xsl:text>
    <fo:inline font-style="italic">
      <xsl:apply-templates/>
    </fo:inline>
    <xsl:text>&#160;»</xsl:text>
  </xsl:template>
</xsl:stylesheet>
