STYLE = stylesheets
BUILD = build
ASSETS = assets
DIST = dist
CONFIG = config

SAXON = saxonb-xslt
SCSS = scss
FOP = fop

PUBLISH = base.css index.html index.atom icon.svg
REMOTE = zamok.crans.org:www/revue/

RELEASE = true

.PHONY: all clean publish serve

all: $(addprefix $(DIST)/, $(PUBLISH))

$(DIST)/index.html: index.xml
	$(SAXON) -xi -s:$< -xsl:$(STYLE)/html.xsl -o:$@ \
		publishedOnly=$(RELEASE)
	$(SAXON) -s:$< -xsl:$(STYLE)/depend.xsl \
		-o:$(BUILD)/index.html.d target=$@

$(DIST)/index.atom: index.xml
	$(SAXON) -xi -s:$< -xsl:$(STYLE)/atom.xsl -o:$@ \
		publishedOnly=$(RELEASE)
	$(SAXON) -s:$< -xsl:$(STYLE)/depend.xsl \
		-o:$(BUILD)/index.atom.d target=$@

$(DIST)/%.css: $(STYLE)/%.scss
	$(SCSS) --sourcemap=none $< $@

$(DIST)/%.svg: $(ASSETS)/%.svg
	cp -f $< $@

serve: $(addprefix $(DIST)/, $(PUBLISH))
	(cd $(DIST); python3 -m http.server)

publish: $(addprefix $(DIST)/, $(PUBLISH))
	scp -r $^ $(REMOTE)

clean:
	rm -f $(BUILD)/index.d $(addprefix $(DIST)/, $(PUBLISH))

-include $(BUILD)/index.html.d
-include $(BUILD)/index.atom.d
