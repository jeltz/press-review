STYLE = stylesheets
BUILD = build
SCHEMAS = schemas
ASSETS = assets
DIST = dist
CONFIG = config

SAXON = saxonb-xslt
JING = jing
SCSS = scss
RSYNC = rsync
XMLLINT = xmllint
FOP = fop
PYTHON = python3

REMOTE = zamok.crans.org:www/revue/
XML = index.html topics.html legal.html index.atom
PUBLISH = $(XML) base.css icon.svg

PUBLISHED_ONLY = $(if $(WIP),false,true)

.PHONY: all clean publish serve

all: $(addprefix $(DIST)/,$(PUBLISH))

$(BUILD)/$(PUBLISHED_ONLY).guard:
	rm -f $(BUILD)/$(if $(filter true,$(PUBLISHED_ONLY)),false,true).guard
	touch $@

$(addprefix $(DIST)/, $(XML)): index.xml $(BUILD)/$(PUBLISHED_ONLY).guard
	$(JING) -c $(SCHEMAS)/index.rnc $<
	$(SAXON) -xi -s:$< -xsl:$(STYLE)/$(notdir $@).xsl -o:$@ \
		published.only=$(PUBLISHED_ONLY)
	$(SAXON) -s:$< -xsl:$(STYLE)/depend.xsl \
		-o:$(BUILD)/$(notdir $@).d target=$@

$(DIST)/%.css: $(STYLE)/%.scss
	$(SCSS) -t expanded --cache-location $(BUILD)/scss \
		--sourcemap=none $< $@

$(DIST)/%.svg: $(ASSETS)/%.svg
	cp -f $< $@

serve: $(addprefix $(DIST)/,$(PUBLISH))
	(cd $(DIST); $(PYTHON) -m http.server)

publish: $(addprefix $(DIST)/,$(PUBLISH))
	$(RSYNC) -avz $^ $(REMOTE)

clean:
	rm -f $(addprefix $(BUILD)/,$(addsuffix .d,$(XML)))
	rm -f $(addprefix $(DIST)/,$(PUBLISH))
	rm -f $(BUILD)/true.guard $(BUILD)/false.guard
	rm -rf $(BUILD)/scss

-include $(addprefix $(BUILD)/, $(addsuffix .d, $(XML)))
