# Revue de presse

## Compilation du site

### Uniquement les éditions publiées

```
$ make all
```

### Avec les éditions non publiées

```
$ make WIP=1
```

## Déploiement

```
$ make publish
```
